# Wedding
Planning and prep for my wedding on June 15, 2019.

## Boards
1. [All tasks](https://gitlab.com/burrowsjeff/wedding/boards/1046124)
2. [Assignee](https://gitlab.com/burrowsjeff/wedding/boards/1046173)
3. [Day by day](https://gitlab.com/burrowsjeff/wedding/boards/1046176)
4. [Month by Month](https://gitlab.com/burrowsjeff/wedding/boards/1046187)